app.controller('buildsController', function($scope, $http) {
    $scope.builds = [];
    $scope.newBuild = {profession: '1',buildname: '', buildlink: '', guildapproved: 'false', notes: '' };
    $scope.addBuildDialog={show: false, error:false};
    $scope.editBuildDialog={show: false, error:false};
    $scope.loginDialog={show:false, username:'', password:'', error:false};
    $scope.editingBuild = {id: '', profession: '1',buildname: '', buildlink: '', guildapproved: 'false', notes: ''};
    $scope.afterLoginAction;
    
    
    //ivan
    $scope.loadBuilds = function(){
    	$http.get("/builds").then(function(response) {
    		$scope.builds=response.data;
        });
    }
    
    $scope.addBuild = function(){
    	$scope.addBuildDialog.error= false;
    	$http.get("/loggedIn").then(function(response){
    		if(response.data.result){
    			$scope.addBuildNoLogin();
    			$scope.loginDialog.show = false;
    		}else{
    			$scope.afterLoginAction = $scope.addBuildNoLogin;
    			$scope.loginDialog.show = true;
    		}
    	});
    }
    
    
    $scope.editBuild = function(id, profession, buildname, buildlink, guildapproved, notes){

    	$scope.editingBuild.id = id;
    	$scope.editingBuild.profession = profession;
    	$scope.editingBuild.buildname = buildname;
    	$scope.editingBuild.buildlink = buildlink;
    	$scope.editingBuild.guildapproved = guildapproved;
    	$scope.editingBuild.notes = notes;
    	console.log($scope.editingBuild.id);
    	console.log($scope.editingBuild.buildname);
    	console.log($scope.editingBuild.profession);
    	console.log($scope.editingBuild.buildlink);
    	console.log($scope.editingBuild.guildapproved);
    	console.log($scope.editingBuild.notes);
    	$scope.editBuildDialog.show = true;
    }
    
    $scope.saveEditedBuild = function(){
    	$scope.editBuildDialog.error= false;
    	$http.get("/loggedIn").then(function(response){
    		if(response.data.result){
    			$scope.saveEditedBuildNoLogin();	
    			$scope.loginDialog.show = false;
    		}else{
    			$scope.afterLoginAction = $scope.saveEditedBuildNoLogin;
    			$scope.loginDialog.show = true;
    		}
    	});
    }
    
    $scope.deleteBuild = function(){
    	$http.get("/loggedIn").then(function(response){
    		if(response.data.result){
    			$scope.deleteBuildNoLogin();	
    			$scope.loginDialog.show = false;
    		}else{
    			$scope.afterLoginAction = $scope.deleteBuildNoLogin;
    			$scope.loginDialog.show = true;
    		}
    	});
    }
    
    //this will still make auth check on the backend, and will not save data, but will not show login dialog
    $scope.addBuildNoLogin = function(){
    	if($scope.newBuild.buildname == "" || $scope.newBuild.buildlink == "" || $scope.newBuild.profession == '1'){
    		 $scope.addBuildDialog.error= true;
    	} else {
    		$scope.addBuildDialog.error= false;
    		$http.post("/builds", $scope.newBuild).then(function(response) {
    			console.log("build added!");
    			$scope.addBuildDialog.show = false;
    			$scope.loadBuilds();
    		});
    	}
    }
    
    //this will still make auth check on the backend, and will not save data, but will not show login dialog
    $scope.saveEditedBuildNoLogin = function(){
    	if($scope.editingBuild.buildname == "" || $scope.editingBuild.buildlink == "" || $scope.editingBuild.profession == '1'){
    		 $scope.editBuildDialog.error= true;
    	} else {
    		$scope.editBuildDialog.error= false;
    		$http.put("/builds", $scope.editingBuild).then(function(response) {
    			console.log("build modified!");
    			$scope.editBuildDialog.show = false;
    			$scope.loadBuilds();
    		});
    	}
    }
    
    //this will still make auth check on the backend, and will not save data, but will not show login dialog
    $scope.deleteBuildNoLogin = function(){
    		console.log($scope.editingBuild.id);
    		$http.delete("/builds/"+$scope.editingBuild.id).then(function(response) {
    			console.log("build deleted!");
    			$scope.editBuildDialog.show = false;
    			$scope.loadBuilds();
    		});
    }
    
    $scope.login = function(){
    	var data = {'username':$scope.loginDialog.username,'password':$scope.loginDialog.password};
    	$http.post("/login", data).then(function(response){
    		$scope.loginDialog.password ='';
    		$scope.loginDialog.error = false;
    		$scope.loginDialog.show = false;
    		$scope.afterLoginAction();
    	},function(response){
    		$scope.loginDialog.password='';
    		$scope.loginDialog.error=true;
    	});
    }
    
    $scope.loadBuilds();
});