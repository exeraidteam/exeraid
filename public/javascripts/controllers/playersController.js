app.controller('playersController', function($scope, $http) {
    $scope.players = []; 
    $scope.builds = [];
    $scope.newPlayer = {name: '', guild: '', ingamename: '', build1Id: 1, build2Id: 1, build3Id: 1};
    $scope.newPlayerBuild = {build1Id: '1', build2Id: '1', build3Id: '1'}
    $scope.addPlayerDialog={show: false, error:false};
    $scope.editPlayerDialog={show: false, error:false};
    $scope.loginDialog={show:false, username:'', password:'', error:false};
    $scope.editedPlayer = {};
    $scope.afterLoginAction;
    $scope.numberOfActive;
    $scope.numberOfPlayers=0;
    
    $scope.getNumberOfActive = function(){
    	$scope.numberOfActive=0;
    	angular.forEach($scope.players, function(player, key) {
    		  if(player.active==true){
    			  $scope.numberOfActive+=1;
    		  }  			  
    		});
    }

    $scope.loadPlayers = function(){
    	$http.get("/players").then(function(response) {
    		//$scope.numberOfActive=0;
    		$scope.players=response.data;
    		$scope.getNumberOfActive();
    		$scope.numberOfPlayers=$scope.players.length;
        });
    }
    $scope.loadBuilds = function(){
    	$http.get("/builds").then(function(response) {
    		$scope.builds=response.data;
        });
    }
    
    $scope.addPlayer = function(){
    	$scope.addPlayerDialog.error = false;
    	$http.get("/loggedIn").then(function(response){
    		if(response.data.result){
    			$scope.addPlayerNoLogin();	
    			$scope.loginDialog.show = false;
    		}else{
    			$scope.afterLoginAction = $scope.addPlayerNoLogin; 
    			$scope.loginDialog.show = true;
    		}
    	});
    }
    
    $scope.editPlayer = function(player){
    	$scope.editedPlayer = player;
    	console.log(player.build1.id);
    	console.log(player.build2.id);
    	console.log(player.build3.id);
    	$scope.editedPlayer.build1Id = parseInt(player.build1.id);
    	$scope.editedPlayer.build2Id = parseInt(player.build2.id);
    	$scope.editedPlayer.build3Id = parseInt(player.build3.id);
    	$scope.editPlayerDialog.show = true;
    }
    
    $scope.deletePlayer = function(){
    	$scope.editPlayerDialog.error= false;
    	$http.get("/loggedIn").then(function(response){
    		if(response.data.result){
    			$scope.deletePlayerNoLogin();	
    			$scope.loginDialog.show = false;
    		}else{
    			$scope.afterLoginAction = $scope.deletePlayerNoLogin;
    			$scope.loginDialog.show = true;
    		}
    	});
    }
    
    $scope.saveEditedPlayer = function(){
    	$scope.editPlayerDialog.error= false;
    	$http.get("/loggedIn").then(function(response){
    		if(response.data.result){
    			console.log("LOGGED IN, CONTINUE TO SAVE..");
    			$scope.saveEditedPlayerNoLogin();	
    			$scope.loginDialog.show = false;
    		}else{
    			$scope.afterLoginAction = $scope.saveEditedPlayerNoLogin;
    			$scope.loginDialog.show = true;
    		}
    	});
    }
    
    $scope.saveEditedPlayerNoLogin = function(){
    	if($scope.editedPlayer.build1Id == '1' || $scope.editedPlayer.name=="" || $scope.editedPlayer.guild=="" || $scope.editedPlayer.ingamename==""){
    		$scope.editPlayerDialog.error = true;	
    	} else {
    		console.log("SAVING...");
    		$scope.editPlayerDialog.error = false;
    		$scope.editedPlayer.build1Id = parseInt( $scope.editedPlayer.build1Id);
    		$scope.editedPlayer.build2Id = parseInt( $scope.editedPlayer.build2Id);
    		$scope.editedPlayer.build3Id = parseInt( $scope.editedPlayer.build3Id);
        	$http.put("/players", $scope.editedPlayer).then(function(response) {
    			console.log("player updated!");
    			$scope.editPlayerDialog.show = false;
    			$scope.loadPlayers();
    		});
    	}
    }
    
  //this will still make auth check on the backend, and will not save data, but will not show login dialog
    $scope.addPlayerNoLogin = function(){
    	if($scope.newPlayerBuild.build1Id == '1' || $scope.newPlayer.name=="" || $scope.newPlayer.guild=="" || $scope.newPlayer.ingamename==""){
    		$scope.addPlayerDialog.error = true;	
    	} else {
    		$scope.addPlayerDialog.error = false;
    		$scope.newPlayer.build1Id = parseInt( $scope.newPlayerBuild.build1Id);
        	$scope.newPlayer.build2Id = parseInt( $scope.newPlayerBuild.build2Id);
        	$scope.newPlayer.build3Id = parseInt( $scope.newPlayerBuild.build3Id);
        	$http.post("/players", $scope.newPlayer).then(function(response) {
    			console.log("player added!");
    			$scope.addPlayerDialog.show = false;
    			$scope.loadPlayers();
    		});
    	}

    }
    
    $scope.deletePlayerNoLogin = function(){
    	$http.delete("/players/"+$scope.editedPlayer.id).then(function(response) {
			console.log("player deleted!");
			$scope.editPlayerDialog.show = false;
			$scope.loadPlayers();
		});
    }
    
    $scope.login = function(){
    	var data = {'username':$scope.loginDialog.username,'password':$scope.loginDialog.password};
    	$http.post("/login", data).then(function(response){
    		$scope.loginDialog.password ='';
    		$scope.loginDialog.error = false;
    		$scope.loginDialog.show = false;
    		if($scope.addPlayerDialog.show){
    			$scope.addPlayerNoLogin();
    		}else{
    			$scope.saveEditedPlayerNoLogin();
    		}
    	},function(response){
    		$scope.loginDialog.password='';
    		$scope.loginDialog.error=true;
    	});
    }
    
    
    $scope.loadPlayers();
    $scope.loadBuilds();
});