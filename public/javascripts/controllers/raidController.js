app.controller('raidController', ['$scope','$http', function($scope, $http) {
	if(id){
		$scope.id = id;
	}else{
		$scope.id = 1;
	}
	console.log("RAID ID:" + $scope.id);
	$scope.editPlayerBuildDialog={show: false, role: {}, partyno: 0, changeMeToAlt: 0};
	$scope.editPlayerDialog={show: false, player: {}, partyno: 0};
	$scope.loginDialog={show:false, username:'', password:'', error:false};
	$scope.raidDialog={show:false}
	$scope.locked="lock";
	$scope.poolfix="poolfree";
			
    $scope.raid = {};
    
    $scope.loadRaid = function(){
    	$http.get("/raids/"+$scope.id).then(function(response) {
    		$scope.raid=response.data;
	    });
    }
    
    $scope.saveRaid = function(){
    	$http.get("/loggedIn").then(function(response){
    		if(response.data.result){
    			$scope.loginDialog.show = false;
    			$scope.raidDialog.show = true;
    		}else{
    			$scope.loginDialog.show = true;
    		}
    	});
    }
    //this will still make auth check on the backend, and will not save data, but will not show login dialog
    $scope.saveRaidNoLogin = function(){
    	if($scope.id==1){
	    	$http.post("/raids", $scope.raid).then(function(response) {
				console.log("saved raid!");
			});
    	}else{
    		$http.put("/raids/"+$scope.id, $scope.raid).then(function(response) {
				console.log("saved raid!");
			});
    	};
    	$scope.raidDialog.show = false;
    }
    
    $scope.login = function(){
    	var data = {'username':$scope.loginDialog.username,'password':$scope.loginDialog.password};
    	$http.post("/login", data).then(function(response){
    		$scope.loginDialog.password ='';
    		$scope.loginDialog.error = false;
    		$scope.loginDialog.show = false;
    		$scope.saveRaidNoLogin();
    	},function(response){
    		$scope.loginDialog.password='';
    		$scope.loginDialog.error=true;
    	});
    }
    
    $scope.changeBuild = function(){
    	$scope.editPlayerBuildDialog.role.buildindex=$scope.editPlayerBuildDialog.changeMeToAlt;
    	$scope.editPlayerBuildDialog.show=false;
    }
    
    $scope.toggleParty = function(){
    	console.log("toggle party");
    	var currentIndex = 0;
    	$.each($scope.raid.parties, function(i, party){
    		if(party.displayed){
    			party.orderno=currentIndex;
    			currentIndex+=1;
    		}
    	});
    }

    
    $scope.resetparty = function(index){
    	$.each($scope.raid.parties[index].roles, function(i, elem){
    		$scope.raid.parties[0].roles.push(elem);
    	});
    	$scope.raid.parties[index].roles=[];
    }
    
    $scope.resetRaid = function(){
    	$.each($scope.raid.parties.slice(1), function(i, party){
    		$.each(party.roles, function (ix, role){
    			$scope.raid.parties[0].roles.push(role);
    		});
    		party.roles=[];
    	});
    }
    
    $scope.newRaid = function(){
    	 if($scope.id != 1){
    		 $scope.id=1;
    		 $scope.loadRaid();
    	 }
    	 else{
    		 $scope.resetRaid();
    	 }
    	 
    }
    
    $scope.lock = function(){
   	 if($scope.locked == "lock"){
   		$scope.locked = "unlock";
   		$scope.poolfix = "poolfixed";
   	 }
   	 else{
   		$scope.locked = "lock";
   		$scope.poolfix = "poolrelative";
   	 }
   	 
   }
 
    $scope.loadRaid();
}]);
