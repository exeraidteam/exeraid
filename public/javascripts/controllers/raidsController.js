app.controller('raidsController', function($scope, $http) {
    $scope.raids = []; // ivan
    $scope.editRaidDialog={show: false, error:false};
    $scope.loginDialog={show:false, username:'', password:'', error:false};
    $scope.editingRaid = {};
    
    //ivan
    $scope.loadRaids = function(){
    	$http.get("/raids").then(function(response) {
    		$scope.raids=response.data;
        });
    }
    
    $scope.editRaid = function(id){
    	console.log(id);
        $http.get("/raids/"+id).then(function(response) {
        	$scope.editingRaid=response.data;
    	});

    	console.log($scope.editingRaid.id);
    	console.log($scope.editingRaid.name);
    	console.log($scope.editingRaid.owner);
    	console.log($scope.editingRaid.created);
    	console.log($scope.editingRaid.notes);
    	$scope.editRaidDialog.show = true;
    }
    
    $scope.saveEditedRaid = function(){
    	$scope.editRaidDialog.error= false;
    	$http.get("/loggedIn").then(function(response){
    		if(response.data.result){
    			$scope.saveEditedRaidNoLogin();	
    			$scope.loginDialog.show = false;
    		}else{
    			$scope.afterLoginAction = $scope.saveEditedRaidNoLogin;
    			$scope.loginDialog.show = true;
    		}
    	});
    }
    
    $scope.deleteRaid = function(){
    	$http.get("/loggedIn").then(function(response){
    		if(response.data.result){
    			$scope.deleteRaidNoLogin();	
    			$scope.loginDialog.show = false;
    		}else{
    			$scope.afterLoginAction = $scope.deleteRaidNoLogin;
    			$scope.loginDialog.show = true;
    		}
    	});
    }
    
    //this will still make auth check on the backend, and will not save data, but will not show login dialog
    $scope.saveEditedRaidNoLogin = function(){
    	if($scope.editingRaid.name == "" || $scope.editingRaid.id == '1'){
    		 $scope.editRaidDialog.error= true;
    	} else {
    		$scope.editRaidDialog.error= false;
    		
        	if($scope.editingRaid.id==1){
    	    	$http.post("/raids", $scope.editingRaid).then(function(response) {
    	    		console.log("raid info modified!");
        			$scope.editRaidDialog.show = false;
        			$scope.loadRaids();
    			});
        	}else{
        		$http.put("/raids/"+$scope.editingRaid.id, $scope.editingRaid).then(function(response) {
        			console.log("raid info modified!");
        			$scope.editRaidDialog.show = false;
        			$scope.loadRaids();
    			});
        	};
    	}
    }
    
    //this will still make auth check on the backend, and will not save data, but will not show login dialog
    $scope.deleteRaidNoLogin = function(){
    		console.log($scope.editingRaid.id);
    		$http.delete("/raids/"+$scope.editingRaid.id).then(function(response) {
    			console.log("raid deleted!");
    			$scope.editRaidDialog.show = false;
    			$scope.loadRaids();
    		});
    }
    
    $scope.login = function(){
    	var data = {'username':$scope.loginDialog.username,'password':$scope.loginDialog.password};
    	$http.post("/login", data).then(function(response){
    		$scope.loginDialog.password ='';
    		$scope.loginDialog.error = false;
    		$scope.loginDialog.show = false;
    		$scope.afterLoginAction();
    	},function(response){
    		$scope.loginDialog.password='';
    		$scope.loginDialog.error=true;
    	});
    }
    
    $scope.loadRaids();
});