app.directive('partyDiv', function() { 
	  return { 
	    restrict: 'EA', 
	    scope: { 
	      party: '=',
	      resetparty: "&"
	    },
	    link: function($scope, element, attrs) {
            $scope.changeMe = function(role, party, index){
            	console.log(role.player.name +":"+party +":" + index);
            	var alterDialog = {};
            	alterDialog.show=true;
            	alterDialog.role=role;
            	alterDialog.partyno=party;
            	alterDialog.changeMeToAlt=index;
            	$scope.$parent.$parent.editPlayerBuildDialog = alterDialog;
            }
            
            $scope.changePartyType = function(newType){
            	$scope.party.partytype=newType;
            }
            
        },
	    templateUrl: 'partyDiv' 
	  }; 
});