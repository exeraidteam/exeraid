app.directive('playerDiv', function() { 
	  return { 
	    restrict: 'EA', 
	    scope: { 
	      party: '=',
	      search: '='
	    }, 
	    link: function($scope, element, attrs) {
            $scope.changeMe = function(role, party, index){
            	var alterDialog = {};
            	alterDialog.show=true;
            	alterDialog.role=role;
            	alterDialog.partyno=party;
            	alterDialog.changeMeToAlt=index;
            	$scope.$parent.editPlayerBuildDialog = alterDialog;
            }
        },
	    templateUrl: 'playerDiv' 
	  }; 
});