function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev, srcX) {
	var srcIndex = srcX!=undefined ? srcX : $(ev.target).scope().party.orderno;
	var draggedId = $(ev.target).scope().x.id; 
    ev.dataTransfer.setData("text", draggedId + ":" + srcIndex);
}

function drop(ev, dstX) {
	ev.preventDefault();
	var destIndex = dstX!=undefined ? dstX : $(ev.target).scope().party.orderno;
	console.log("DROP ON PARTY:" + destIndex);
	var identifier = ev.dataTransfer.getData("text").split(':');
	var id = identifier[0];
	var srcIndex = identifier[1];
	if(srcIndex == destIndex){
		return false;
	}
	var scope = $('#indexdiv').scope();
    var data = $("#"+id).scope().x; 
	var foundSource = false;
	var foundDest = false;
    $.each(scope.raid.parties, function(i, elem){
    	if(elem.orderno==srcIndex && elem.displayed && !foundSource){
    		srcIndex = i;
			foundSource = true;
    	}
    	if(elem.orderno==destIndex && elem.displayed && ! foundDest){
    		destIndex=i;
			foundDest = true;
    	}
    });
    data.orderno=scope.raid.parties[destIndex].roles.length;
    scope.raid.parties[destIndex].roles.push(data);
    var i = scope.raid.parties[srcIndex].roles.indexOf(data);
    if (i > -1) {
    	scope.raid.parties[srcIndex].roles.splice(i, 1);
    }
    scope.$apply();
}

function dropOnPlayer(ev, dstX){
	ev.preventDefault();
	ev.stopPropagation();
	var dstIndex = dstX!=undefined ? dstX : $(ev.target).scope().party.orderno;
	var identifier = ev.dataTransfer.getData("text").split(':');
	var id = identifier[0];
	var srcIndex = identifier[1];
	
	var scope = $('#indexdiv').scope();
    var srcRole = $("#"+id).scope().x;
    var dstRole = $(ev.target).scope().x;
	var foundSource = false;
	var foundDest = false;
    $.each(scope.raid.parties, function(i, elem){
    	if(elem.orderno==srcIndex && elem.displayed && !foundSource){
    		srcIndex=i;
			foundSource = true;
    	};
    	if(elem.orderno==dstIndex && elem.displayed && !foundDest){
    		dstIndex=i;
			foundDest = true;
    	};
    });
    var dstRoles = scope.raid.parties[dstIndex].roles;
    var srcRoles= scope.raid.parties[srcIndex].roles;
    
    var dstRoleIndex = dstRoles.indexOf(dstRole);
    var srcRoleIndex = srcRoles.indexOf(srcRole);
    
    
    if(srcIndex == dstIndex){
    	var srcOrderNo = srcRole.orderno; 
    	srcRole.orderno=dstRole.orderno;
    	dstRole.orderno=srcOrderNo;
    }else{
    	//remove player from source player list
    	srcRoles.splice(srcRoleIndex, 1);
    	//update orderno of all players behind the source player    	
    	$.each(srcRoles, function(index, role){
    		if(role.orderno>srcRole.orderno){
    			role.orderno = role.orderno-1;
    		}
	    });
    	var dstOrderNo = dstRole.orderno;
    	$.each(dstRoles, function(index, role){
    		if(role.orderno>=dstOrderNo){
    			role.orderno = role.orderno+1;
    		}
	    });
    	srcRole.orderno=dstOrderNo;
    	dstRoles.push(srcRole);
    }
    scope.$apply();
	
}