# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table BUILD (
  ID                        bigint auto_increment not null,
  PROFESSION                varchar(255),
  BUILDNAME                 varchar(255),
  BUILDLINK                 varchar(255),
  GUILDAPPROVED             boolean,
  OWNER_ID                  bigint,
  NOTES                     varchar(255),
  CREATED                   timestamp,
  constraint pk_BUILD primary key (ID))
;

create table PARTY (
  ID                        bigint auto_increment not null,
  ORDERNO                   integer,
  PARTYTYPE                 integer,
  DISPLAYED                 boolean,
  RAID_ID                   bigint,
  constraint pk_PARTY primary key (ID))
;

create table PLAYER (
  ID                        bigint auto_increment not null,
  USER_ID                   bigint,
  ACTIVE                    boolean,
  NAME                      varchar(255),
  GUILD                     varchar(255),
  INGAMENAME                varchar(255),
  BUILD1_ID                 bigint,
  BUILD2_ID                 bigint,
  BUILD3_ID                 bigint,
  constraint pk_PLAYER primary key (ID))
;

create table RAID (
  ID                        bigint auto_increment not null,
  NAME                      varchar(255),
  CREATED                   timestamp,
  OWNER_ID                  bigint,
  NOTES                     varchar(255),
  constraint pk_RAID primary key (ID))
;

create table ROLE (
  ID                        bigint auto_increment not null,
  ORDERNO                   integer,
  PLAYER_ID                 bigint,
  BUILDINDEX                integer,
  PARTY_ID                  bigint,
  constraint pk_ROLE primary key (ID))
;

create table USERS (
  ID                        bigint auto_increment not null,
  EMAIL                     varchar(255),
  PASSWORD                  varchar(255),
  NAME                      varchar(255),
  PRIVILEGE                 integer,
  CREATED                   timestamp,
  constraint pk_USERS primary key (ID))
;

alter table BUILD add constraint fk_BUILD_owner_1 foreign key (OWNER_ID) references USERS (ID) on delete restrict on update restrict;
create index ix_BUILD_owner_1 on BUILD (OWNER_ID);
alter table PARTY add constraint fk_PARTY_raid_2 foreign key (RAID_ID) references RAID (ID) on delete restrict on update restrict;
create index ix_PARTY_raid_2 on PARTY (RAID_ID);
alter table PLAYER add constraint fk_PLAYER_user_3 foreign key (USER_ID) references USERS (ID) on delete restrict on update restrict;
create index ix_PLAYER_user_3 on PLAYER (USER_ID);
alter table PLAYER add constraint fk_PLAYER_build1_4 foreign key (BUILD1_ID) references BUILD (ID) on delete restrict on update restrict;
create index ix_PLAYER_build1_4 on PLAYER (BUILD1_ID);
alter table PLAYER add constraint fk_PLAYER_build2_5 foreign key (BUILD2_ID) references BUILD (ID) on delete restrict on update restrict;
create index ix_PLAYER_build2_5 on PLAYER (BUILD2_ID);
alter table PLAYER add constraint fk_PLAYER_build3_6 foreign key (BUILD3_ID) references BUILD (ID) on delete restrict on update restrict;
create index ix_PLAYER_build3_6 on PLAYER (BUILD3_ID);
alter table RAID add constraint fk_RAID_owner_7 foreign key (OWNER_ID) references USERS (ID) on delete restrict on update restrict;
create index ix_RAID_owner_7 on RAID (OWNER_ID);
alter table ROLE add constraint fk_ROLE_player_8 foreign key (PLAYER_ID) references PLAYER (ID) on delete restrict on update restrict;
create index ix_ROLE_player_8 on ROLE (PLAYER_ID);
alter table ROLE add constraint fk_ROLE_party_9 foreign key (PARTY_ID) references PARTY (ID) on delete restrict on update restrict;
create index ix_ROLE_party_9 on ROLE (PARTY_ID);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists BUILD;

drop table if exists PARTY;

drop table if exists PLAYER;

drop table if exists RAID;

drop table if exists ROLE;

drop table if exists USERS;

SET REFERENTIAL_INTEGRITY TRUE;

