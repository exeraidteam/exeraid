import java.util.List;

import models.Build;
import models.Party;
import models.Player;
import models.Role;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import dao.DaoFactory;
import dao.build.BuildDao;
import dao.build.CsvBuildDaoImpl;
import dao.build.JdbcBuildDaoImpl;
import dao.party.PartyDao;
import dao.player.CsvPlayerDaoImpl;
import dao.player.EbeanPlayerDaoImpl;
import dao.player.JdbcPlayerDaoImpl;
import dao.player.PlayerDao;
import dao.role.RoleDao;


public class Global extends GlobalSettings{
	
	public void onStart(Application app){
		Logger.info("APPLICATION STARTED");
		JdbcBuildDaoImpl buildDao = new JdbcBuildDaoImpl();
		int bldCount = buildDao.getBuildCount(); 
		if(bldCount == 0){
			Logger.info("Found no players in the database, importing...");
			BuildDao csvBuildDao = new CsvBuildDaoImpl();
			List<Build> csvBuilds = csvBuildDao.getAllBuilds();
			for(Build build : csvBuilds){
//				Logger.info("Found player:" + player);
				buildDao.addBuild(build);
			}
		}
		
//		RaidsDao raidsDao = DaoFactory.getRaidDao();
//		Raid onlyRaid = raidsDao.find(1L);
		
		PartyDao partyDao = DaoFactory.getPartyDao();
		Party party = partyDao.find(1L);
	
		RoleDao roleDao = DaoFactory.getRoleDao();
		JdbcPlayerDaoImpl playerDao = new JdbcPlayerDaoImpl();
		
		int count = playerDao.getPlayerCount(); 
		if(count == 0){
			Logger.info("Found no players in the database, importing...");
			PlayerDao csvPlayerDao = new CsvPlayerDaoImpl();
			List<Player> csvPlayers = csvPlayerDao.getAllPlayers();
			for(Player player : csvPlayers){
				
//				Logger.info("Found player:" + player);
				playerDao.addPlayer(player);
				
				EbeanPlayerDaoImpl ebeanPlayerDaoImpl = new EbeanPlayerDaoImpl();
				Player fromEbean = ebeanPlayerDaoImpl.findPlayer(player.getId());
				
				Role role = new Role();
				role.setBuildindex(0);
				role.setOrderno(0);
				role.setPlayer(fromEbean);
				role.setParty(party);
				roleDao.addRole(role);
				
				
			}
		}
		 
	}
}
