package dao.party;

import models.Party;

public interface PartyDao {
	public Party find(Long id);
}
