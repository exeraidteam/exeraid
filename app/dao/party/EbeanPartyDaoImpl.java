package dao.party;

import models.Party;

import com.avaje.ebean.Ebean;

public class EbeanPartyDaoImpl implements PartyDao {
	
	public Party find(Long id) {
		return Ebean.find(Party.class, id);
	}

}
