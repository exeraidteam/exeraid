package dao.raid;

import java.util.List;
import models.Raid;

public interface RaidDao {
	
	public List<Raid> getAllRaids();
	
	public Raid find(Long id);
	
	public boolean update(Raid raid);
	
	public void save(Raid raid);
	
	public boolean delete(Long id);
}
