package dao.raid;

import java.util.List;

import models.Build;
import models.Raid;

import com.avaje.ebean.Ebean;

public class EbeanRaidDaoImpl implements RaidDao {

	public List<Raid> getAllRaids() {
		List<Raid> raids = Ebean.find(Raid.class).fetch("owner").orderBy("id").findList();
		return raids;
	}
	
	public Raid find(Long id) {
		return Ebean.find(Raid.class).fetch("parties").
				fetch("parties.roles").
				fetch("parties.roles.player").
				fetch("parties.roles.player.build1").
				fetch("parties.roles.player.build2").
				fetch("parties.roles.player.build3").
				where().eq("id", id).findUnique();
	}

	public boolean update(Raid raid) {
		Ebean.update(raid);
		return true;
	}
	
	public void save(Raid raid) {
		Ebean.save(raid);
	}
	
	public boolean delete(Long id) {
		Ebean.delete(Raid.class, id);
		return true;
	}

}
