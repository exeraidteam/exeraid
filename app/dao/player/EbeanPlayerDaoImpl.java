package dao.player;

import java.util.List;

import models.Player;

import com.avaje.ebean.Ebean;

public class EbeanPlayerDaoImpl implements PlayerDao{

	public List<Player> getAllPlayers() {
		List<Player> players = Ebean.find(Player.class).fetch("build1").fetch("build2").fetch("build3").orderBy("name").findList();
		return players;
	}

	public Player addPlayer(Player player) {
		Ebean.save(player);
		return player;
	}

	public boolean updatePlayer(Player player) {
		Ebean.update(player);
		return true;
	}

	public boolean deletePlayer(Long id) {
		Ebean.delete(Player.class, id);
		return true;
	}

	public Player findPlayer(Long id) {
		return Ebean.find(Player.class, id);
	}

}
