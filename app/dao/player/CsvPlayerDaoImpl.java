package dao.player;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import controllers.Assets;
import models.Player;

public class CsvPlayerDaoImpl implements PlayerDao {

	public List<Player> getAllPlayers() {
		List<Player> players = new ArrayList<Player>();

		// Delimiter used in CSV file
		final String DELIMITER = ",";
		try (BufferedReader fileReader = new BufferedReader(new FileReader(
				play.api.Play.getFile("public/resources/players.csv",
						play.api.Play.current())))) {
			String line = "";

			// Read the file line by line
			int index = 0;
			while ((line = fileReader.readLine()) != null) {
				index++;
				if (index == 1) {
					continue;
				} else {
					// Get all tokens available in line
					String[] tokens = line.split(DELIMITER);
					Player newPlayer = new Player(Long.valueOf(index-1), Long.valueOf(tokens[0]), 
								Boolean.valueOf(tokens[1]),
								tokens[2],
								tokens[3],
								tokens[4],
								Long.valueOf(tokens[5]),
								Long.valueOf(tokens[6]),
								Long.valueOf(tokens[7]));
					
					players.add(newPlayer);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return players;
	}

	@Override
	public Player addPlayer(Player player) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean updatePlayer(Player player) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deletePlayer(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Player findPlayer(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}