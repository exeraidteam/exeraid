package dao.player;

import java.util.List;

import models.Player;

public interface PlayerDao {
	
	public List<Player> getAllPlayers();
	
	public Player addPlayer(Player player);
	
	public boolean updatePlayer(Player player);
	
	public boolean deletePlayer(Long id);
	
	public Player findPlayer(Long id);

}
