package dao.player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import models.Player;
import play.Logger;
import play.db.DB;

public class JdbcPlayerDaoImpl  {

/*	public List<Player> getAllPlayers() {
		Logger.info("getAllPlayers");
		List<Player> players = new ArrayList<Player>();
		String getAllSql = "SELECT * FROM PLAYER";
		try (Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement(getAllSql);
			ResultSet rs = ps.executeQuery()) {
			while(rs.next()){
				Player player = new Player();
				player.setId(rs.getLong("ID"));
				player.setActive(rs.getBoolean("Active"));
				player.setPlays(rs.getString("PLAYS"));
				player.setName(rs.getString("NAME"));
				player.setIngameName(rs.getString("INGAMENAME"));
				
				player.setMainProfession(rs.getString("MAINPROFESSION"));
				player.setMainBuildName(rs.getString("MAINBUILDNAME"));
				player.setMainBuildLink(rs.getString("MAINBUILDLINK"));
				
				player.setAlt1Profession(rs.getString("ALT1PROFESSION"));
				player.setAlt1BuildName(rs.getString("ALT1BUILDNAME"));
				player.setAlt1BuildLink(rs.getString("ALT1BUILDLINK"));
				
				player.setAlt2Profession(rs.getString("ALT2PROFESSION"));
				player.setAlt2BuildName(rs.getString("ALT2BUILDNAME"));
				player.setAlt2BuildLink(rs.getString("ALT2BUILDLINK"));
				
				players.add(player);
			}
			
		}catch(Exception e){
			Logger.error("Exception fetching players", e);
		}
		
		return players;
	}

 */
	public Player addPlayer(Player player) {
		Logger.info("addPlayer");
		
		
		String insertSQL = "INSERT INTO PLAYER"
				+ "(USER_ID, ACTIVE, NAME, INGAMENAME, GUILD, BUILD1_ID, BUILD2_ID, BUILD3_ID, ID) VALUES "
				+ "(?,?,?,?,?,?,?,?,?)";
		try (Connection con = DB.getConnection();
			
			PreparedStatement ps = con.prepareStatement(insertSQL)) {
			ps.setLong(1, player.getUserId());
			ps.setBoolean(2, player.getActive());
			ps.setString(3, player.getName());
			ps.setString(4, player.getIngamename());			
			ps.setString(5, player.getGuild());
			ps.setLong(6, player.getBuild1Id());
			ps.setLong(7, player.getBuild2Id());
			ps.setLong(8, player.getBuild3Id());
			ps.setLong(9, player.getId());
			ps.executeUpdate();
			
			ResultSet tableKeys = ps.getGeneratedKeys();
			tableKeys.next();
			long id = tableKeys.getLong(1);
			player.setId(id);
			tableKeys.close();
		} catch (Exception e) {
			Logger.error("Exception adding player", e);
		}
		return player;
	}

	/*
	public boolean updatePlayer(Player player) {
		Logger.info("updatePlayer");
		String updateSql = "UPDATE PLAYER SET ACTIVE=?, PLAYS=?, NAME=?, INGAMENAME=?, "
				+ "MAINPROFESSION=?, MAINBUILDNAME=?, MAINBUILDLINK=? "
				+ "ALT1PROFESSION=?, ALT1BUILDNAME=?, ALT1BUILDLINK=? "
				+ "ALT2PROFESSION=?, ALT2BUILDNAME=?, ALT2BUILDLINK=? "
				+ "WHERE ID=?";
		try (Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement(updateSql)) {
			
			ps.setBoolean(1, player.getActive());
			ps.setString(2, player.getPlays());
			ps.setString(3, player.getName());
			ps.setString(4, player.getIngameName());
			
			ps.setString(5, player.getMainProfession());
			ps.setString(6, player.getMainBuildName());
			ps.setString(7, player.getMainBuildLink());
			
			ps.setString(8, player.getAlt1Profession());
			ps.setString(9, player.getAlt1BuildName());
			ps.setString(10, player.getAlt1BuildLink());
			
			ps.setString(11, player.getAlt2Profession());
			ps.setString(12, player.getAlt2BuildName());
			ps.setString(13, player.getAlt2BuildLink());
			
			ps.setLong(14, player.getId());
			ps.executeUpdate();
		
		} catch (Exception e) {
			Logger.error("Exception updating player", e);
			return false;
		}
		return true;
	}

	public boolean deletePlayer(Long id) {
		Logger.info("deletePlayer");
		String deleteSql = "DELETE FROM PLAYER WHERE ID=?";
		try (Connection con = DB.getConnection();
				PreparedStatement ps = con.prepareStatement(deleteSql)) {
			con.setAutoCommit(true);
			ps.setLong(1, id);
			ps.execute();
			
		}catch(Exception e){
			Logger.error("Exception updating player", e);
			return false;
		}
		return true;
	}

	public Player findPlayer(Long id) {
		Logger.info("findPlayer");
		Player player = null;
		String findSql = "SELECT * FROM PLAYER WHERE ID=?";
		try (Connection con = DB.getConnection();
				PreparedStatement ps = con.prepareStatement(findSql)) {
			con.setAutoCommit(true);
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				player = new Player();
				player.setId(rs.getLong("ID"));
				player.setActive(rs.getBoolean("Active"));
				player.setPlays(rs.getString("PLAYS"));
				player.setName(rs.getString("NAME"));
				player.setIngameName(rs.getString("INGAMENAME"));
				
				player.setMainProfession(rs.getString("MAINPROFESSION"));
				player.setMainBuildName(rs.getString("MAINBUILDNAME"));
				player.setMainBuildLink(rs.getString("MAINBUILDLINK"));
				
				player.setAlt1Profession(rs.getString("ALT1PROFESSION"));
				player.setAlt1BuildName(rs.getString("ALT1BUILDNAME"));
				player.setAlt1BuildLink(rs.getString("ALT1BUILDLINK"));
				
				player.setAlt2Profession(rs.getString("ALT2PROFESSION"));
				player.setAlt2BuildName(rs.getString("ALT2BUILDNAME"));
				player.setAlt2BuildLink(rs.getString("ALT2BUILDLINK"));
				
			}
			rs.close();
		}catch(Exception e){
			Logger.error("Exception updating player", e);
		}
		return player;
	}
*/	
	public int getPlayerCount(){
		Logger.info("getPlayerCount");
    	int count = 0;
    	String findSql = "SELECT COUNT(*) FROM PLAYER";
		try (Connection con = DB.getConnection();
				PreparedStatement ps = con.prepareStatement(findSql)) {
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				count = rs.getInt(1);
			}
			rs.close();
		}catch(Exception e){
			Logger.error("Exception fetching player count", e);
		}
		Logger.info("Count of players in the db:" + count);
		return count;
    }

}
