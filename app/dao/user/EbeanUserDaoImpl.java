package dao.user;


import models.User;

import org.mindrot.jbcrypt.BCrypt;

import com.avaje.ebean.Ebean;

public class EbeanUserDaoImpl implements UserDao {

	public boolean authenticate(String username, String password) {
//		return false;
		User targetUser = Ebean.find(User.class).where().eq("name", username.toLowerCase()).findUnique();
		if(targetUser == null){
			return false;
		}else{
			return BCrypt.checkpw(password, targetUser.getPassword()); 
		}
	}

}
