package dao.user;

public interface UserDao {

	public boolean authenticate(String username, String password);
}
