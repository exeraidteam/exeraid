package dao.role;

import java.util.List;

import models.Role;

public interface RoleDao {
	public Role addRole(Role role);
	
	public void updateRole(Role role);
	
	public boolean deleteRole(Long id);
	
	public List<Role> findRolesInPoolForRaid(Long partyId);
	
	public List<Role> findRolesInPartiesForRaid(Long partyId);

}
