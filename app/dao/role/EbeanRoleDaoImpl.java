package dao.role;

import java.util.List;

import models.Role;

import com.avaje.ebean.Ebean;


public class EbeanRoleDaoImpl implements RoleDao {

	public Role addRole(Role role) {
		Ebean.save(role);
		return role;
	}

	public void updateRole(Role role) {
		Ebean.save(role);
	}

	public boolean deleteRole(Long id) {
		Ebean.delete(Role.class, id);
		return true;
	}

	@Override
	public List<Role> findRolesInPoolForRaid(Long partyId) {
		List<Role> roles = Ebean.createNamedQuery(Role.class, "findRolesInPoolForRaid")
        .setParameter("id", partyId)
        .findList();
	
		return roles;
	}

	@Override
	public List<Role> findRolesInPartiesForRaid(Long partyId) {
		List<Role> roles = Ebean.createNamedQuery(Role.class, "findRolesInPoolForRaid")
		   .setParameter("id", partyId)
		    .findList();
			
		return roles;
	}

}
