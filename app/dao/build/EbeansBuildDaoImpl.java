package dao.build;

import java.util.List;

import play.Logger;

import com.avaje.ebean.Ebean;

import models.Build;
import models.Raid;

public class EbeansBuildDaoImpl implements BuildDao{

	public List<Build> getAllBuilds() {
		//List<Build> builds = Ebean.find(Build.class).orderBy("profession").findList();
		List<Build> builds = Ebean.find(Build.class).fetch("owner").orderBy("profession").findList();
		return builds;
	}
	public Build find(Long id){
		return Ebean.find(Build.class, id);
	}

	public Build addBuild(Build build) {
		Ebean.save(build);
		return build;
	}

	public boolean updateBuild(Build build) {
		Ebean.update(build);
		return true;
	}

	public boolean deleteBuild(Long id) {
		Ebean.delete(Build.class, id);
		return true;
	}

	public Build findBuild(Long id) {
		return Ebean.find(Build.class, id);
	}
	
	

}
