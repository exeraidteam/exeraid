package dao.build;

import java.util.List;

import models.Build;

public interface BuildDao {

	public List<Build> getAllBuilds();
	
	public Build find(Long id);
	
	public Build addBuild(Build build);
	
	public boolean updateBuild(Build build);
	
	public boolean deleteBuild(Long id);
	
}
