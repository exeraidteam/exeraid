package dao.build;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import models.Build;

public class CsvBuildDaoImpl implements BuildDao {

	public List<Build> getAllBuilds() {
		List<Build> builds = new ArrayList<Build>();

		// Delimiter used in CSV file
		final String DELIMITER = ",";
		try (BufferedReader fileReader = new BufferedReader(new FileReader(
				play.api.Play.getFile("public/resources/builds.csv",
						play.api.Play.current())))) {
			String line = "";

			// Read the file line by line
			int index = 0;
			while ((line = fileReader.readLine()) != null) {
				index++;
				if (index == 1) {
					continue;
				} else {
					// Get all tokens available in line
					String[] tokens = line.split(DELIMITER);
//					profession,buildName,buildLink,guildapproved,owner_id,notes
					Build newBuild = new Build(Long.valueOf(index-1),tokens[0], 
									tokens[1],
									tokens[2],
									Boolean.valueOf(tokens[3]),
								Long.valueOf(tokens[4]), tokens[5]);
					builds.add(newBuild);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return builds;
	}
	
	public Build addBuild(Build build) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public boolean updateBuild(Build build){
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean deleteBuild(Long id) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public Build find(Long id){
		// TODO Auto-generated method stub
		return null;
	}

}
