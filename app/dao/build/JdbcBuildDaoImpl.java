package dao.build;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import com.avaje.ebean.Ebean;

import play.Logger;
import play.db.DB;
import models.Build;

public class JdbcBuildDaoImpl implements BuildDao {

	@Override
	public List<Build> getAllBuilds() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Build find(Long id){
		List<Build> builds = this.getAllBuilds();
		for(Build build:builds){
			if(build.getId() == id)
				return build;
		}
		return null;
	}

	@Override
	public Build addBuild(Build build) {
		String insertSQL = "INSERT INTO BUILD"
				+ "(ID, PROFESSION, BUILDNAME, BUILDLINK, GUILDAPPROVED, OWNER_ID, NOTES) VALUES "
				+ "(?,?,?,?,?,?,?)";
		try (Connection con = DB.getConnection();
			
			PreparedStatement ps = con.prepareStatement(insertSQL)) {
			ps.setLong(1, build.getId());
			ps.setString(2, build.getProfession());
			ps.setString(3, build.getBuildname());
			ps.setString(4, build.getBuildlink());
			ps.setBoolean(5, build.getGuildapproved());
			ps.setLong(6, build.getOwnerId());
			ps.setString(7, build.getNotes());
			ps.executeUpdate();
			
			ResultSet tableKeys = ps.getGeneratedKeys();
			tableKeys.next();
			long id = tableKeys.getLong(1);
			build.setId(id);
			tableKeys.close();
		} catch (Exception e) {
			Logger.error("Exception adding build", e);
		}
		return build;
	}
	
	public int getBuildCount(){
		Logger.info("getBuildCount");
    	int count = 0;
    	String findSql = "SELECT COUNT(*) FROM BUILD";
		try (Connection con = DB.getConnection();
				PreparedStatement ps = con.prepareStatement(findSql)) {
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				count = rs.getInt(1);
			}
			rs.close();
		}catch(Exception e){
			Logger.error("Exception fetching build count", e);
		}
		Logger.info("Count of builds in the db:" + count);
		return count;
    }
	
	public boolean updateBuild(Build build){
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean deleteBuild(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

}
