package dao;

import dao.build.BuildDao;
import dao.build.EbeansBuildDaoImpl;
import dao.party.EbeanPartyDaoImpl;
import dao.party.PartyDao;
import dao.player.EbeanPlayerDaoImpl;
import dao.player.PlayerDao;
import dao.raid.EbeanRaidDaoImpl;
import dao.raid.RaidDao;
import dao.role.EbeanRoleDaoImpl;
import dao.role.RoleDao;
import dao.user.EbeanUserDaoImpl;
import dao.user.UserDao;

public class DaoFactory {
	private static PlayerDao playerDao;
	private static PartyDao partyDao;
	private static RoleDao roleDao;
	private static RaidDao raidDao;
	private static BuildDao buildDao;
	private static UserDao userDao;
	
	public static synchronized BuildDao getBuildDao(){
		if(buildDao== null){
			buildDao = new EbeansBuildDaoImpl();
		}
		return buildDao;
	}
	
	public static synchronized PlayerDao getPlayerDao(){
		if(playerDao==null){
			playerDao = new EbeanPlayerDaoImpl();
		}
		return playerDao;
	}
	
	public static synchronized PartyDao getPartyDao(){
		if(partyDao==null){
			partyDao = new EbeanPartyDaoImpl();
		}
		return partyDao;
	}
	
	public static synchronized RoleDao getRoleDao(){
		if(roleDao==null){
			roleDao = new EbeanRoleDaoImpl();
		}
		return roleDao;
	}
	
	public static synchronized RaidDao getRaidDao(){
		if(raidDao== null){
			raidDao = new EbeanRaidDaoImpl();
		}
		return raidDao;
	}
	
	public static synchronized UserDao getUserDao(){
		if(userDao== null){
			userDao = new EbeanUserDaoImpl();
		}
		return userDao;
	}


}
