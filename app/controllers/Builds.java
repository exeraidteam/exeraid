package controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import models.Build;
import models.Player;
import models.User;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import dao.DaoFactory;
import dao.build.BuildDao;
import dao.player.PlayerDao;

public class Builds extends Controller {
	
    public Result findAll(){
    	BuildDao buildDao = DaoFactory.getBuildDao();
		List<Build> builds = buildDao.getAllBuilds();
		String json = Ebean.json().toJson(builds);
    	return ok(json);
    }
    
    public Result saveAll(){
    	try{
    		ArrayNode nodeArray = (ArrayNode)request().body().asJson();
    		List<Build> builds = new ArrayList<Build>();
    		Logger.info("SIZE:" + nodeArray.size());
	    	for(JsonNode node: nodeArray){
	    		Build build = Json.fromJson(node, Build.class);
	    		builds.add(build);
	    	}
	    	return ok();
    	}catch(Exception e){
    		Logger.error("Exception", e);
    		return internalServerError("Problem");
    	}
    	
    	
    }
    

    @Security.Authenticated(Secured.class)
	public Result save(){
		String json = request().body().asJson().toString();
		Build build = Ebean.json().toBean(Build.class, json);
		build.setId(null);
		String username = session("username");
		User targetUser = Ebean.find(User.class).where().eq("name", username.toLowerCase()).findUnique();
		build.setOwner(targetUser);
		build.setCreated(Calendar.getInstance().getTime());
		DaoFactory.getBuildDao().addBuild(build);
		return ok();
	}
    
    @Security.Authenticated(Secured.class)
	public Result edit(){
		String json = request().body().asJson().toString();
		Build build = Ebean.json().toBean(Build.class, json);
		String username = session("username");
		User targetUser = Ebean.find(User.class).where().eq("name", username.toLowerCase()).findUnique();
		build.setOwner(targetUser);
		build.setCreated(Calendar.getInstance().getTime());
		DaoFactory.getBuildDao().updateBuild(build);
		return ok();
	}
    
    @Security.Authenticated(Secured.class)
	public Result delete(long id){
    	Build build = DaoFactory.getBuildDao().find(1L);
    	PlayerDao playerDao = DaoFactory.getPlayerDao();
    	List<Player> players = playerDao.getAllPlayers();
    	for (Player player : players) {
    		if(player.getBuild1().getId() == id){
    			player.setBuild1(build);
    		}
    		if(player.getBuild2().getId() == id){
    			player.setBuild2(build);
    		}
    		if(player.getBuild3().getId() == id){
    			player.setBuild3(build);
    		}
    		if(player.getBuild1().getId() == 1 && player.getBuild2().getId() != 1){
    			player.setBuild1(player.getBuild2());
    			player.setBuild2(player.getBuild3());
    			player.setBuild3(build);
    		} else {
    			if(player.getBuild1().getId() == 1 && player.getBuild3().getId() != 1){
        			player.setBuild1(player.getBuild3());
        			player.setBuild3(build);
        		}
    		}

    		playerDao.updatePlayer(player);
    	}
		DaoFactory.getBuildDao().deleteBuild(id);
		return ok();
	}
    
    
}
