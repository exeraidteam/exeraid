package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Party;
import models.Player;
import models.Build;
import models.Role;
import models.User;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import dao.DaoFactory;
import dao.build.BuildDao;
import dao.party.PartyDao;
import dao.player.PlayerDao;

public class Players extends Controller {
	
    public Result findAll(){
    	PlayerDao playerDao = DaoFactory.getPlayerDao();
		List<Player> players = playerDao.getAllPlayers();
		String json = Ebean.json().toJson(players);
    	return ok(json);
    }
    
    public Result saveAll(){
    	try{
    		ArrayNode nodeArray = (ArrayNode)request().body().asJson();
    		List<Player> players = new ArrayList<Player>();
    		Logger.info("SIZE:" + nodeArray.size());
	    	for(JsonNode node: nodeArray){
	    		Player player = Json.fromJson(node, Player.class);
	    		players.add(player);
	    	}
	    	return ok();
    	}catch(Exception e){
    		Logger.error("Exception", e);
    		return internalServerError("Problem");
    	}
    	
    	
    }
    
    @Security.Authenticated(Secured.class)
   	public Result save(){
   		String json = request().body().asJson().toString();
    	Logger.info(json);
   		Player player = Ebean.json().toBean(Player.class, json);
   		player.setId(null);
   		player.setUserId(1L);
   		player.setActive(true);
   		
   		BuildDao buildDao = DaoFactory.getBuildDao();
		List<Build> builds = buildDao.getAllBuilds();
		Build[] roleBuilds = new Build[3];
		for (Build build : builds) {
		    if(build.getId()==player.getBuild1Id()){
		    	player.setBuild1(build);
		    	roleBuilds[0] = build;
		    }
		    if(build.getId()==player.getBuild2Id()){
		    	player.setBuild2(build);
		    	roleBuilds[1] = build;
		    }
		    if(build.getId()==player.getBuild3Id()){
		    	player.setBuild3(build);
		    	roleBuilds[2] = build;
		    }
		}
		
		//Vidi komentar ispod
		String username = session("username");
		User targetUser = Ebean.find(User.class).where().eq("name", username.toLowerCase()).findUnique();
		player.setUser(targetUser);
		/* OK, ovo ce za sada da radi ali mozda dodati polje da kada se kreira player da moze da mu se doda user.
		 * Kako za sada imamo samo par usera ovo nije potrebno ali treba ubaciti u backlog.
		 */
		
   		DaoFactory.getPlayerDao().addPlayer(player);
   		
   		Role role = new Role();
   		role.setBuildindex(0);
   		role.setOrderno(0);
   		role.setPlayer(player);
   		//role.setBuilds(roleBuilds);
   		role.setId(null);
   		PartyDao partyDao = DaoFactory.getPartyDao();
   		Party party = partyDao.find(1L);
   		role.setParty(party);
   		DaoFactory.getRoleDao().addRole(role);
   		return ok();
   	}
    
    
    @Security.Authenticated(Secured.class)
	public Result edit(){
		String json = request().body().asJson().toString();
		Player player = Ebean.json().toBean(Player.class, json);
		BuildDao buildDao = DaoFactory.getBuildDao();
		player.setBuild1(buildDao.find(player.getBuild1Id()));
		player.setBuild2(buildDao.find(player.getBuild2Id()));
		player.setBuild3(buildDao.find(player.getBuild3Id()));
		DaoFactory.getPlayerDao().updatePlayer(player);
		return ok();
	}
    
    @Security.Authenticated(Secured.class)
	public Result delete(long id){
    	PlayerDao playerDao = DaoFactory.getPlayerDao();
    	Player player = playerDao.findPlayer(id);
    	player.setActive(false);
    	playerDao.updatePlayer(player);
   		return ok();
	}
    
    
}
