package controllers;

import java.util.List;

import models.Build;
import models.Party;
import models.Raid;
import models.Role;
import models.User;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;

import dao.DaoFactory;
import dao.raid.RaidDao;

public class Raids extends Controller {

	public Result find(Long id){
		Raid raid = DaoFactory.getRaidDao().find(id);
		
		for(Party party : raid.getParties()){
			party.setRaid(null);
			for(Role role: party.getRoles()){
				Build[] builds = new Build[3];
				builds[0] = role.getPlayer().getBuild1();
				builds[1] = role.getPlayer().getBuild2();
				builds[2] = role.getPlayer().getBuild3();
				role.setBuilds(builds);
			}
		}
		JsonNode json = Json.toJson(raid);
		return ok(json);
	}
	
    public Result findAll(){
    	RaidDao raidDao = DaoFactory.getRaidDao();
		List<Raid> raids = raidDao.getAllRaids();
		String json = Ebean.json().toJson(raids);
    	return ok(json);
    }
	
	@Security.Authenticated(Secured.class)
	public Result update(Long id){
		String json = request().body().asJson().toString();
		Raid raid = Ebean.json().toBean(Raid.class, json);
		DaoFactory.getRaidDao().update(raid);
		return ok();
	}
	
	@Security.Authenticated(Secured.class)
	public Result save(){
		String json = request().body().asJson().toString();
		Raid raid = Ebean.json().toBean(Raid.class, json);
		raid.setId(null);

		String username = session("username");
		User targetUser = Ebean.find(User.class).where().eq("name", username.toLowerCase()).findUnique();
		raid.setOwner(targetUser);
		
		if(raid.getName().equals("DEFAULT")){
			raid.setName(targetUser.getName().substring(0, 1).toUpperCase() + targetUser.getName().substring(1) + "'s Raid");
		}
		
		if(raid.getNotes().equals("Default Raid")){
			raid.setNotes("This is " + targetUser.getName().substring(0, 1).toUpperCase() + targetUser.getName().substring(1) + "'s Raid.");
		}
		
		
		for(Party party: raid.getParties()){
			party.setId(null);
			for(Role role: party.getRoles()){
				role.setId(null);
			}
		}
		DaoFactory.getRaidDao().save(raid);
		return ok();
	}
	
    @Security.Authenticated(Secured.class)
	public Result delete(long id){
    	if(id!= 1L){
    		DaoFactory.getRaidDao().delete(id);
    	}
		
		return ok();
	}
}
