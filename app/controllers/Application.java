package controllers;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.builds;
import views.html.buildsDiv;
import views.html.index;
import views.html.partyDiv;
import views.html.playerDiv;
import views.html.players;
import views.html.raids;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import dao.DaoFactory;

public class Application extends Controller {
	
	public Result loggedIn(){
		boolean loggedIn = session().containsKey("username");
		ObjectNode result = Json.newObject();
	    result.put("result", loggedIn);
		return ok(result);
	}
	
	public Result login(){
		JsonNode json = request().body().asJson();
		String username = json.findPath("username").textValue();
		String password = json.findPath("password").textValue();;
		boolean success = DaoFactory.getUserDao().authenticate(username, password);
		if(success){
			session("username", username);
			return ok();
		}else{
			return badRequest();
		}
	}
	
	public Result logout() {
	    session().clear();
	    return redirect(
	        routes.Application.index()
	    );
	}
    
    public Result index() {
        return ok(index.render("1"));
    }
    
    public Result indexRaid(Long id) {
        return ok(index.render(String.valueOf(id)));
    }
    
    public Result playerDiv() {
        return ok(playerDiv.render());
    }
    
    public Result partyDiv() {
        return ok(partyDiv.render());
    }
    
    public Result builds() {
    	return ok(builds.render("Test"));
    }
    
    public Result buildsDiv() {
    	return ok(buildsDiv.render());
    }
    
    public Result raids() {
    	return ok(raids.render("Test"));
    }
    
    public Result players() {
    	return ok(players.render("Test"));
    }
}
