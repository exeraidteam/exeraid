package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "BUILD")
public class Build {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	private Long id;
	
	@Column(name = "PROFESSION")
	private String profession;
	
	@Column(name = "BUILDNAME")
	private String buildname;
	
	@Column(name = "BUILDLINK")
	private String buildlink;
	
	@Column(name = "GUILDAPPROVED")
	private Boolean guildapproved;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "OWNER_ID", referencedColumnName = "ID")
	private User owner;

	@Column(name = "NOTES")
	private String notes;
	
	@Column(name = "CREATED")
	private Date created;
	
	@JsonIgnore
	@Transient
	private Long ownerId;

	
	public Build(){}
	
	public Build(Long id, String profession, String buildName, String buildLink, Boolean guildapproved, Long ownerId, String notes){
		this.id = id;
		this.profession = profession;
		this.buildname = buildName;
		this.buildlink = buildLink;
		this.guildapproved = guildapproved;
		this.ownerId = ownerId;
		this.notes = notes;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getBuildname() {
		return buildname;
	}

	public void setBuildname(String buildname) {
		this.buildname = buildname;
	}

	public String getBuildlink() {
		return buildlink;
	}

	public void setBuildlink(String buildlink) {
		this.buildlink = buildlink;
	}

	public Boolean getGuildapproved() {
		return guildapproved;
	}

	public void setGuildapproved(Boolean guildapproved) {
		this.guildapproved = guildapproved;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	
	
}
