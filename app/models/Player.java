package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="PLAYER")
public class Player {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "USER_ID", referencedColumnName = "ID")
	private User user;
	
	@Column(name = "ACTIVE")
	private Boolean active;	
	@Column(name = "NAME")
	private String name;
	@Column(name = "GUILD")
	private String guild;
	@Column(name="INGAMENAME")
	private String ingamename;
	
	@ManyToOne
	@JoinColumn(name = "BUILD1_ID", referencedColumnName = "ID")
	private Build build1;
	
	@ManyToOne
	@JoinColumn(name = "BUILD2_ID", referencedColumnName = "ID")
	private Build build2;
	
	@ManyToOne
	@JoinColumn(name = "BUILD3_ID", referencedColumnName = "ID")
	private Build build3;

	@Transient
	private Long userId;
	@Transient
	private Long build1Id;
	@Transient
	private Long build2Id;
	@Transient
	private Long build3Id;
	
	public Player(){}
	
	public Player(Long id, Long userId, Boolean active, String name, String guildname, String ingamename, Long build1Id, Long build2Id, Long build3Id){
		this.id = id;
		this.userId = userId;
		this.active = active;
		this.name = name;
		this.guild = guildname;
		this.ingamename = ingamename;
		this.build1Id = build1Id;
		this.build2Id = build2Id;
		this.build3Id = build3Id;
		
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Boolean getActive() {
		return active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getGuild() {
		return guild;
	}


	public void setGuild(String guild) {
		this.guild = guild;
	}


	public String getIngamename() {
		return ingamename;
	}


	public void setIngamename(String ingamename) {
		this.ingamename = ingamename;
	}


	public Build getBuild1() {
		return build1;
	}


	public void setBuild1(Build build1) {
		this.build1 = build1;
	}


	public Build getBuild2() {
		return build2;
	}


	public void setBuild2(Build build2) {
		this.build2 = build2;
	}


	public Build getBuild3() {
		return build3;
	}


	public void setBuild3(Build build3) {
		this.build3 = build3;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getBuild1Id() {
		return build1Id;
	}

	public void setBuild1Id(Long build1Id) {
		this.build1Id = build1Id;
	}

	public Long getBuild2Id() {
		return build2Id;
	}

	public void setBuild2Id(Long build2Id) {
		this.build2Id = build2Id;
	}

	public Long getBuild3Id() {
		return build3Id;
	}

	public void setBuild3Id(Long build3Id) {
		this.build3Id = build3Id;
	}

	@Override
	public String toString() {
		return "id:" + id + ", name:" + name + ", userid:" + userId + ", active:" + active +
				",guild:" + guild + ", ingamename:" + ingamename + ", build1Id:" + build2Id + ", build2Id:" + build2Id + ",build3Id:" + build3Id;
	}
	
	
}
