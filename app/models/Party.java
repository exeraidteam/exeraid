package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "PARTY")
public class Party {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	@Column(name="ID")
	private Long id;
	
	@Column(name="ORDERNO")
	private Integer orderno;
	
	// 0 - pool
	// 1 - melee
	// 2 - ranged
	// 3 - focus
	// 4 - special
	@Column(name="PARTYTYPE")
	private int partytype;
	
	@Column(name="DISPLAYED")
	private Boolean displayed;
	
	@ManyToOne
	@JoinColumn(name = "RAID_ID", referencedColumnName = "ID")
	private Raid raid;
	
	@OneToMany(mappedBy="party", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Role> roles;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getOrderno() {
		return orderno;
	}
	public void setOrderno(Integer orderno) {
		this.orderno = orderno;
	}
	public int getPartyType() {
		return partytype;
	}
	public void setPartyType (int type) {
		this.partytype = type;
	}
	public Boolean getDisplayed() {
		return displayed;
	}
	public void setDisplayed (boolean displayed) {
		this.displayed = displayed;
	}
	public Raid getRaid() {
		return raid;
	}
	public void setRaid(Raid raid) {
		this.raid = raid;
	}
	public int getPartytype() {
		return partytype;
	}
	public void setPartytype(int partytype) {
		this.partytype = partytype;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	public void setDisplayed(Boolean displayed) {
		this.displayed = displayed;
	}
	
	
}
