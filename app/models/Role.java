package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@NamedQueries(value={
	    @NamedQuery(
	      name="findRolesInPoolForRaid",
	      query="find * fetch player fetch player.build1 fetch player.build2 fetch player.build3 where party_id=:id and orderno=0"),
	    @NamedQuery(
	      name="findRolesInPartiesForRaid",
	      query="find * fetch player where party_id=:id and orderno!=0")
	    }
)
@Entity
@Table(name = "ROLE")
public class Role {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "ORDERNO")
	private Integer orderno;
	
	@ManyToOne
	@JoinColumn(name = "PLAYER_ID", referencedColumnName = "ID")
	private Player player;
	
	@Column(name = "BUILDINDEX")
	private Integer buildindex;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "PARTY_ID", referencedColumnName = "ID")
	private Party party;

	@Transient
	private Build[] builds;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getOrderno() {
		return orderno;
	}

	public void setOrderno(Integer orderno) {
		this.orderno = orderno;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Integer getBuildindex() {
		return buildindex;
	}

	public void setBuildindex(Integer buildindex) {
		this.buildindex = buildindex;
	}

	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	public Build[] getBuilds() {
		return builds;
	}

	public void setBuilds(Build[] builds) {
		this.builds = builds;
	}
	
	

}
